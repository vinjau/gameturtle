package rs.ac.bg.etf.student.pm130681d.gameturtle.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.GameStatus;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class Game implements Serializable {

    private int id;
    private String name;
    private String description;
    private String place;
    private Date date;
    private int minPlayers;
    private int maxPlayers;
    private GameStatus status;

    @SerializedName("media")
    private List<GameMedia> medias;

    public List<GameMedia> getMedias() {
        return medias;
    }

    public void setMedias(List<GameMedia> medias) {
        this.medias = medias;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public void setMinPlayers(int minPlayers) {
        this.minPlayers = minPlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }
}
