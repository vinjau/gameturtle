package rs.ac.bg.etf.student.pm130681d.gameturtle.view.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import rs.ac.bg.etf.student.pm130681d.gameturtle.R;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Player;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.PlayerVoteIndicatorWrapper;
import rs.ac.bg.etf.student.pm130681d.gameturtle.view.PlayerListFragment;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class PlayerVoteListAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater = null;
    private List<PlayerVoteIndicatorWrapper> players;
    private PlayerListFragment playerListFragment;

    public PlayerVoteListAdapter(Context context, List<PlayerVoteIndicatorWrapper> players, PlayerListFragment playerListFragment) {
        this.context = context;
        this.players = players;
        this.playerListFragment = playerListFragment;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int position) {
        return players.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.player_list_item, null);

        final PlayerVoteIndicatorWrapper playerWrapper = players.get(position);

        if (playerWrapper != null) {
            TextView name = (TextView) vi.findViewById(R.id.playerListName);
            final TextView minus = (TextView) vi.findViewById(R.id.playerListMinus);
            final TextView plus = (TextView) vi.findViewById(R.id.playerListNamePlus);
            name.setText(playerWrapper.getPlayer().getUserId().getUsername());
            if (playerWrapper.isVoted()) {
                plus.setText(playerWrapper.getPlayer().getUserId().getPositiveVotes() + "");
                minus.setText(playerWrapper.getPlayer().getUserId().getNegativeVotes() + "");
            } else {
                plus.setVisibility(View.INVISIBLE);
                minus.setVisibility(View.INVISIBLE);
                final Button plusButton = (Button) vi.findViewById(R.id.votePositive);
                final Button minusButton = (Button) vi.findViewById(R.id.voteNegative);
                plusButton.setVisibility(View.VISIBLE);
                minusButton.setVisibility(View.VISIBLE);
                plusButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        playerListFragment.voteFor(playerWrapper.getPlayer(), 1);
                        plusButton.setVisibility(View.INVISIBLE);
                        minusButton.setVisibility(View.INVISIBLE);
                        plus.setVisibility(View.VISIBLE);
                        minus.setVisibility(View.VISIBLE);
                        plus.setText((playerWrapper.getPlayer().getUserId().getPositiveVotes() + 1) + "");
                        minus.setText(playerWrapper.getPlayer().getUserId().getNegativeVotes() + "");

                    }
                });
                minusButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        playerListFragment.voteFor(playerWrapper.getPlayer(), -1);
                        plusButton.setVisibility(View.INVISIBLE);
                        minusButton.setVisibility(View.INVISIBLE);
                        plus.setVisibility(View.VISIBLE);
                        minus.setVisibility(View.VISIBLE);
                        plus.setText(playerWrapper.getPlayer().getUserId().getPositiveVotes() + "");
                        minus.setText((playerWrapper.getPlayer().getUserId().getNegativeVotes() + 1) + "");
                    }
                });
            }
        }

        return vi;
    }
}

