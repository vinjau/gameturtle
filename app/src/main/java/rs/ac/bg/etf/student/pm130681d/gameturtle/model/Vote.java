package rs.ac.bg.etf.student.pm130681d.gameturtle.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.VoteStatus;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.VoteType;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class Vote implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("vote")
    private VoteType vote;

    @SerializedName("status")
    private VoteStatus status;

    @SerializedName("playerId")
    private Player playerId;

    @SerializedName("userId")
    private User userId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public VoteStatus getStatus() {
        return status;
    }

    public void setStatus(VoteStatus status) {
        this.status = status;
    }

    public Player getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Player playerId) {
        this.playerId = playerId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public VoteType getVote() {
        return vote;
    }

    public void setVote(VoteType vote) {
        this.vote = vote;
    }
}
