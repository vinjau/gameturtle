package rs.ac.bg.etf.student.pm130681d.gameturtle.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.GameInstanceStatus;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.GameInstanceType;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class GameInstance implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("game")
    private Game game;

    @SerializedName("place")
    private String place;

    @SerializedName("status")
    private GameInstanceStatus status;

    @SerializedName("type")
    private GameInstanceType type;

    @SerializedName("players")
    private List<Player> players;

    @SerializedName("shouts")
    private List<ShoutBox> shouts;

    @SerializedName("date")
    private Long date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public GameInstanceStatus getStatus() {
        return status;
    }

    public void setStatus(GameInstanceStatus status) {
        this.status = status;
    }

    public GameInstanceType getType() {
        return type;
    }

    public void setType(GameInstanceType type) {
        this.type = type;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public List<ShoutBox> getShouts() {
        return shouts;
    }

    public void setShouts(List<ShoutBox> shouts) {
        this.shouts = shouts;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }
}
