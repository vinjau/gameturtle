package rs.ac.bg.etf.student.pm130681d.gameturtle.view;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import rs.ac.bg.etf.student.pm130681d.gameturtle.AppCompatPreferenceActivity;
import rs.ac.bg.etf.student.pm130681d.gameturtle.R;
import rs.ac.bg.etf.student.pm130681d.gameturtle.SettingsActivity;
import rs.ac.bg.etf.student.pm130681d.gameturtle.controller.GameController;
import rs.ac.bg.etf.student.pm130681d.gameturtle.controller.UserController;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Game;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.GameInstance;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.ShoutBox;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.User;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Vote;
import rs.ac.bg.etf.student.pm130681d.gameturtle.view.helper.GameInstanceListAdapter;
import rs.ac.bg.etf.student.pm130681d.gameturtle.view.helper.GameListAdapter;
import rs.ac.bg.etf.student.pm130681d.gameturtle.view.helper.MyAccountActivity;


public class GameListActivity extends AppCompatActivity
        implements UserController.ViewInterface, GameController.ViewInterface {

    public static final String GAME_INSTANCE = "gameturtle.view.GameListActivity.GameInstance";
    private static final String TAG = "GameListActivity";

    private static final String MY_ACCOUNT = "My Account";
    private static final String CURRENT_GAMES = "Current games";
    private static final String PAST_GAMES = "Past games";
    private static final String LIST_GAMES = "List games";
    private static final String SETTINGS = "Settings";
    private static final String LOGOUT = "Logout";
    private static final String GLOBAL = "Global";

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    private GameController gameController;
    private UserController userController;

    private String lastList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_list);

        gameController = new GameController(this, this);
        userController = new UserController(this, this);

        lastList = GLOBAL;

        drawerLayout = (DrawerLayout) findViewById(R.id.gameListDrawer);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_account: {
                        userController.getUser();
                        drawerLayout.closeDrawers();
                        return true;
                    }
                    case R.id.nav_current_games: {
                        userController.getCurrentGames();
                        drawerLayout.closeDrawers();
                        return true;
                    }
                    case R.id.nav_past_games: {
                        userController.getPastGames();
                        drawerLayout.closeDrawers();
                        return true;
                    }
                    case R.id.nav_list_games: {
                        gameController.searchBoardGames(null);
                        drawerLayout.closeDrawers();
                        return true;
                    }
                    case R.id.nav_settings: {
                        Intent intent = new Intent(GameListActivity.this, SettingsActivity.class);
                        startActivity(intent);
                        drawerLayout.closeDrawers();
                        return true;
                    }
                    case R.id.nav_logout: {
                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(GameListActivity.this);
                        sp.edit().remove(UserController.ACCESS_TOKEN).apply();
                        sp.edit().remove(UserController.USERNAME).apply();
                        Intent intent = new Intent(GameListActivity.this, RegisterActivity.class);
                        startActivity(intent);
                        finish();
                        drawerLayout.closeDrawers();
                        return true;
                    }
                }
                return false;
            }
        });

        handleIntent(getIntent());

    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
        super.onNewIntent(intent);
    }

    @Override
    public boolean onSearchRequested() {
        doMySearch("m");
        return super.onSearchRequested();
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            doMySearch(query);
        }
    }

    private void doMySearch(String query) {
        lastList = "Search";
        gameController.getGames(query);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        menu.getItem(0).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                return true;
            }
        });

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (gameController == null) {
            gameController = new GameController(this, this);
        }
        if (userController == null) {
            userController = new UserController(this, this);
        }
        if (lastList == null) {
            lastList = GLOBAL;
        }
        switch (lastList) {
            case GLOBAL:
                gameController.getGames(null);
                break;
            case PAST_GAMES:
                userController.getPastGames();
                break;
            case CURRENT_GAMES:
                userController.getCurrentGames();
                break;
            case LIST_GAMES:
                gameController.searchBoardGames(null);
                break;
        }

    }

    @Override
    public void onSearchBoardGames(final List<Game> games) {
        lastList = LIST_GAMES;
        ListView listView = (ListView) findViewById(R.id.gameListViewId);
        listView.setAdapter(new GameListAdapter(this, games));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Game game = games.get(i);
                Intent intent = new Intent(getBaseContext(), GameActivity.class);
                intent.putExtra(GameActivity.GAME, game);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onGetGames(final List<GameInstance> games) {
        ListView listView = (ListView) findViewById(R.id.gameListViewId);
        listView.setAdapter(new GameInstanceListAdapter(this, games));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                GameInstance gameInstance = games.get(i);
                Intent intent = new Intent(getBaseContext(), SingleGameInstanceActivity.class);
                intent.putExtra(GAME_INSTANCE, gameInstance);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onGameHosted() {

    }

    @Override
    public void onGameStarted() {

    }

    @Override
    public void onGameFinished() {

    }

    @Override
    public void onGameJoined() {

    }

    @Override
    public void onGameLeave() {

    }

    @Override
    public void onGameShout() {

    }

    @Override
    public void onGetGame(GameInstance gameInstance) {

    }

    @Override
    public void onGetShouts(List<ShoutBox> body) {

    }

    @Override
    public void onKickPlayer(int playerId) {

    }

    @Override
    public void onError(String s, String reason) {

    }

    @Override
    public void onRegister() {

    }

    @Override
    public void onLogin() {

    }

    @Override
    public void onUpdate() {

    }

    @Override
    public void onGetUser(User user) {
        Intent intent = new Intent(this, MyAccountActivity.class);
        intent.putExtra(MyAccountActivity.USER, user);
        startActivity(intent);
    }

    @Override
    public void onGetPastGames(final List<GameInstance> games) {
        lastList = PAST_GAMES;
        ListView listView = (ListView) findViewById(R.id.gameListViewId);
        listView.setAdapter(new GameInstanceListAdapter(this, games));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                GameInstance gameInstance = games.get(i);
                Intent intent = new Intent(getBaseContext(), SingleGameInstanceActivity.class);
                intent.putExtra(GAME_INSTANCE, gameInstance);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onGetCurrentGames(final List<GameInstance> games) {
        lastList = CURRENT_GAMES;
        ListView listView = (ListView) findViewById(R.id.gameListViewId);
        listView.setAdapter(new GameInstanceListAdapter(this, games));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                GameInstance gameInstance = games.get(i);
                Intent intent = new Intent(getBaseContext(), SingleGameInstanceActivity.class);
                intent.putExtra(GAME_INSTANCE, gameInstance);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onGetUserVotes(List<Vote> votes) {

    }

    @Override
    public void onUserVote() {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFail() {

    }
}
