package rs.ac.bg.etf.student.pm130681d.gameturtle.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import rs.ac.bg.etf.student.pm130681d.gameturtle.R;
import rs.ac.bg.etf.student.pm130681d.gameturtle.controller.UserController;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.GameInstance;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Player;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.User;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Vote;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.GameInstanceStatus;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.PlayerStatus;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.PlayerType;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.PlayerVoteIndicatorWrapper;
import rs.ac.bg.etf.student.pm130681d.gameturtle.view.helper.PlayerListAdapter;
import rs.ac.bg.etf.student.pm130681d.gameturtle.view.helper.PlayerVoteListAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PlayerListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PlayerListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PlayerListFragment extends Fragment implements UserController.ViewInterface {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "PlayerListFragment";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private List<Player> players;
    private List<PlayerVoteIndicatorWrapper> playersWrapper;
    private UserController userController;
    private ListView playerListView;
    private GameInstance gameInstance;

    private OnFragmentInteractionListener mListener;

    public PlayerListFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PlayerListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PlayerListFragment newInstance(List<Player> players, GameInstance gameInstance) {
        PlayerListFragment fragment = new PlayerListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, (Serializable) players);
        args.putSerializable(ARG_PARAM2, gameInstance);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            players = (List<Player>) getArguments().getSerializable(ARG_PARAM1);
            gameInstance = (GameInstance) getArguments().getSerializable(ARG_PARAM2);
        }
        userController = new UserController(getContext(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_player_list, container, false);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        final String hostUsername = sp.getString(UserController.USERNAME, "");

        //Namestiti za vote
        if (players != null) {
            playerListView = (ListView) v.findViewById(R.id.playerListViewId);
            if (gameInstance.getStatus().equals(GameInstanceStatus.CREATED)) {
                for (Player player : players) {
                    if (hostUsername.equals(player.getUserId().getUsername()) && player.getType().equals(PlayerType.HOST)) {
                        playerListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                            @Override
                            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                                TextView textView = (TextView) view.findViewById(R.id.playerListName);
                                String username = textView.getText().toString();
                                if (!username.equals(hostUsername)){
                                    for (final Player player : players) {
                                        if (player.getUserId().getUsername().equals(username)) {
                                            Log.d(TAG, "onItemLongClick: " + username + " " + player.getId());
                                            AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                                            alertDialog.setTitle("Kick?");
                                            alertDialog.setMessage("Are you sure you want to kick " + username);
                                            DialogInterface.OnClickListener dialogOnClick = new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int which) {
                                                    if (which == AlertDialog.BUTTON_POSITIVE) {
                                                        mListener.onCallForPlayerKick(player.getId());
                                                    }
                                                    dialogInterface.dismiss();
                                                }
                                            };
                                            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", dialogOnClick);
                                            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", dialogOnClick);
                                            alertDialog.show();
                                        }
                                    }
                                }
                                return false;
                            }
                        });
                    }
                }
            }
        }
            if (!gameInstance.getStatus().equals(GameInstanceStatus.FINISHED)) {
                playerListView.setAdapter(new PlayerListAdapter(getContext(), players));
            } else {
                userController.getUserVote(gameInstance.getId());
            }
        return v;
    }

    public void disableKick() {
        playerListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                return false;
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void addPlayer() {
        userController.getUser();
    }

    @Override
    public void onRegister() {

    }

    @Override
    public void onLogin() {

    }

    @Override
    public void onUpdate() {

    }

    @Override
    public void onGetUser(User user) {
        if (playerListView != null) {
            for (Player player : players) {
                if (player.getUserId().getUsername().equals(user.getUsername())) {
                    // leave game
                    players.remove(player);
                    playerListView.invalidateViews();
                    return;
                }
            }
            Player player = new Player();
            player.setType(PlayerType.PLAYER);
            player.setStatus(PlayerStatus.JOINED);
            player.setUserId(user);
            players.add(player);
            playerListView.invalidateViews();
        }
    }

    @Override
    public void onGetPastGames(List<GameInstance> games) {

    }

    @Override
    public void onGetCurrentGames(List<GameInstance> games) {

    }

    @Override
    public void onGetUserVotes(List<Vote> votes) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        String username = sp.getString(UserController.USERNAME, "");
        boolean playerInsterted = false;
        playersWrapper = new ArrayList<>();
        for(Player player : players) {
            playerInsterted = false;
            if (player.getUserId().getUsername().equals(username)) {
                PlayerVoteIndicatorWrapper pw = new PlayerVoteIndicatorWrapper();
                pw.setPlayer(player);
                pw.setVoted(true);
                playersWrapper.add(pw);
                continue;
            }
            for (Vote vote : votes) {
                if (player.getId() == vote.getPlayerId().getId()) {
                    PlayerVoteIndicatorWrapper pw = new PlayerVoteIndicatorWrapper();
                    pw.setPlayer(player);
                    pw.setVoted(true);
                    playersWrapper.add(pw);
                    playerInsterted = true;
                    break;
                }
            }
            if (!playerInsterted) {
                PlayerVoteIndicatorWrapper pw = new PlayerVoteIndicatorWrapper();
                pw.setPlayer(player);
                pw.setVoted(false);
                playersWrapper.add(pw);
            }
        }
        playerListView.setAdapter(new PlayerVoteListAdapter(getContext(), playersWrapper, this));
        // mozda view.invalidate
    }

    @Override
    public void onUserVote() {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFail() {

    }

    public void setGameInstance(GameInstance gameInstance) {
        this.gameInstance = gameInstance;
    }

    public void setupVotes() {
        userController.getUserVote(gameInstance.getId());
    }

    public void removePlayer() {
        userController.getUser();
    }

    public void voteFor(Player player, int vote) {
        userController.vote(gameInstance.getId(), vote, player.getId());
    }

    public void kickPlayer(int playerId) {
        Player playerToKick = null;
        for(Player player : players) {
            if (player.getId() == playerId) {
                playerToKick = player;
                break;
            }
        }
        if (playerToKick != null) {
            players.remove(playerToKick);
            playerListView.invalidateViews();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onCallForPlayerKick(int playerId);
    }
}
