package rs.ac.bg.etf.student.pm130681d.gameturtle.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import rs.ac.bg.etf.student.pm130681d.gameturtle.R;
import rs.ac.bg.etf.student.pm130681d.gameturtle.controller.ApiInterface;
import rs.ac.bg.etf.student.pm130681d.gameturtle.controller.GameController;
import rs.ac.bg.etf.student.pm130681d.gameturtle.controller.UserController;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Game;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.GameInstance;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Player;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.ShoutBox;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.GameInstanceStatus;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.PlayerType;

public class SingleGameInstanceActivity extends AppCompatActivity
        implements
        PlayerListFragment.OnFragmentInteractionListener,
        ChatFragment.OnFragmentInteractionListener,
        GameController.ViewInterface {
    private static final String TAG = "SingleGameInstanceActivity";
    private static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.GERMANY);
    private PlayerListFragment playerListFragment;
    private ChatFragment chatFragment;
    private String username;
    private GameController gameController;
    private GameInstance gameInstance;
    private Button startButton;
    private TextView status;
    private TextView date;
    private TextView place;
    private TextView host;
    private TextView gameName;
    private TextView hostPlus;
    private TextView hostMinus;
    private TextView numberOfPlayers;
    private Button joinLeaveButton;
    private Button chatButton;
    private ImageView imageView;
    private boolean isHost;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_game_instance);

        status = (TextView) findViewById(R.id.singleGameInstanceStatus);
        date = (TextView) findViewById(R.id.singleGameInstanceDate);
        place = (TextView) findViewById(R.id.singleGameInstancePlace);
        startButton = (Button) findViewById(R.id.singleGameInstanceStartButton);
        imageView = (ImageView) findViewById(R.id.singleGameInstanceImageId);
        joinLeaveButton = (Button) findViewById(R.id.singleGameInstanceButton);
        host = (TextView) findViewById(R.id.singleGameInstanceHostName);
        gameName = (TextView) findViewById(R.id.singleGameInstanceNameId);
        hostPlus = (TextView) findViewById(R.id.singleGameInstancePlus);
        hostMinus = (TextView) findViewById(R.id.singleGameInstanceMinus);
        numberOfPlayers = (TextView) findViewById(R.id.singleGameInstanceNumberOfPlayers);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        username = sp.getString(UserController.USERNAME, "");

        Intent intent = getIntent();
        gameInstance = (GameInstance) intent.getSerializableExtra(GameListActivity.GAME_INSTANCE);
        gameController = new GameController(this, this);
        gameController.getGame(gameInstance.getId());

        try {
            if (gameInstance.getGame().getMedias().get(0).getMedia() != null) {
                Glide.with(this)
                        .load(ApiInterface.API_URL + gameInstance.getGame().getMedias().get(0).getMedia())
                        .into(imageView);
            }
        } catch (Exception e) {
        }

    }

    private void setupFields() {
        startButton.setVisibility(View.INVISIBLE);
        place.setText("Place: " + gameInstance.getPlace());
        status.setText("Game status: " + gameInstance.getStatus().toString());
        date.setText(dateFormat.format(gameInstance.getDate()));

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SingleGameInstanceActivity.this, GameActivity.class);
                intent.putExtra(GameActivity.GAME, gameInstance.getGame());
                startActivity(intent);
            }
        });

        if (!gameInstance.getStatus().equals(GameInstanceStatus.CREATED)) {
            joinLeaveButton.setVisibility(View.INVISIBLE);
        }

        if (!gameInstance.getStatus().equals(GameInstanceStatus.FINISHED)) {
            for (Player player : gameInstance.getPlayers()) {
                if (player.getUserId().getUsername().equals(username) && player.getType().equals(PlayerType.HOST)) {
                    startButton.setVisibility(View.VISIBLE);
                    isHost = true;
                    if (gameInstance.getStatus().equals(GameInstanceStatus.STARTED)) {
                        startButton.setText("FINISH");
                    }
                    break;
                }
            }
        }

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("START".equals(startButton.getText().toString())) {
                    gameController.start(gameInstance.getId());
                }
                if ("FINISH".equals(startButton.getText().toString())) {
                    gameController.finish(gameInstance.getId());
                    startButton.setVisibility(View.INVISIBLE);
                }
                if (playerListFragment != null) {
                    playerListFragment.disableKick();
                }
            }
        });
    }

    @Override
    public void onCallForPlayerKick(int playerId) {
        gameController.kickPlayer(playerId);
    }

    @Override
    public void onSearchBoardGames(List<Game> games) {

    }

    @Override
    public void onGetGames(List<GameInstance> games) {

    }

    @Override
    public void onGameHosted() {

    }

    @Override
    public void onGameStarted() {
        joinLeaveButton.setVisibility(View.INVISIBLE);
        startButton.setText("FINISH");
        status.setText("Game status: STARTED");
    }

    @Override
    public void onGameFinished() {
        status.setText("Game status: FINISHED");
        if (playerListFragment != null) {
            if (gameInstance != null) {
                gameInstance.setStatus(GameInstanceStatus.FINISHED);
                playerListFragment.setGameInstance(gameInstance);
            }
            if (playerListFragment.isAdded()) {
                playerListFragment.setupVotes();
            }
        }
    }

    @Override
    public void onGameJoined() {
        if (playerListFragment != null) {
            if (gameInstance.getPlayers().size() < gameInstance.getGame().getMaxPlayers()) {
                joinLeaveButton.setText("LEAVE");
                chatButton.setVisibility(View.VISIBLE);
                playerListFragment.addPlayer();
                numberOfPlayers.setText((gameInstance.getPlayers().size() + 1) + "/" + gameInstance.getGame().getMaxPlayers());
            } else {
                Toast.makeText(SingleGameInstanceActivity.this, "There is no spot left.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onGameLeave() {
        if (playerListFragment != null) {
            if (isHost) {
                finish();
            }
            joinLeaveButton.setText("JOIN");
            chatButton.setVisibility(View.INVISIBLE);
            if (!playerListFragment.isInLayout()) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.singleGameInstanceFrame, playerListFragment)
                        .commit();
            }
            playerListFragment.removePlayer();
            numberOfPlayers.setText((gameInstance.getPlayers().size() - 1) + "/" + gameInstance.getGame().getMaxPlayers());
        }

    }

    @Override
    public void onGameShout() {

    }

    @Override
    public void onGetGame(final GameInstance gameInstance) {
        this.gameInstance = gameInstance;
        setupFields();
        playerListFragment = PlayerListFragment.newInstance(gameInstance.getPlayers(), gameInstance);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.singleGameInstanceFrame, playerListFragment)
                .commit();
        if (gameInstance == null) {
            finish();
        } else {
            numberOfPlayers.setText(gameInstance.getPlayers().size() + "/" + gameInstance.getGame().getMaxPlayers());

            joinLeaveButton = (Button) findViewById(R.id.singleGameInstanceButton);
            chatButton = (Button) findViewById(R.id.singleGameInstanceButtonSwitch);

            for (Player player : gameInstance.getPlayers()) {
                if (player.getType().equals(PlayerType.HOST)) {
                    host.setText(player.getUserId().getUsername());
                    hostPlus.setText(player.getUserId().getPositiveVotes() + "");
                    hostMinus.setText(player.getUserId().getNegativeVotes() + "");
                }
                if (player.getUserId().getUsername().equals(username)) {
                    joinLeaveButton.setText("LEAVE");
                }
            }
            gameName.setText(gameInstance.getGame().getName());

            joinLeaveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (joinLeaveButton.getText().toString().equals("JOIN")) {
                            gameController.join(gameInstance.getId());
                    } else {
                        gameController.leave(gameInstance.getId());
                    }
                }
            });

            chatButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (chatButton.getText().toString().equals("CHAT")) {
                        if (chatFragment == null) {
                            chatFragment = ChatFragment.newInstance(gameInstance.getShouts());
                        }
                        gameController.getShouts(gameInstance.getId());
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.singleGameInstanceFrame, chatFragment)
                                .commit();
                        chatButton.setText("PLAYERS");
                    } else {
                        if (playerListFragment == null) {
                            playerListFragment = PlayerListFragment.newInstance(gameInstance.getPlayers(), gameInstance);
                        }
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.singleGameInstanceFrame, playerListFragment)
                                .commit();
                        chatButton.setText("CHAT");
                    }
                }
            });
        }
    }

    @Override
    public void onGetShouts(List<ShoutBox> body) {
        if (chatFragment != null) {
            chatFragment.updateList(body);
        }
    }

    @Override
    public void onKickPlayer(int playerId) {
        if (playerListFragment != null) {
            setupNumberOfPlayers();
            playerListFragment.kickPlayer(playerId);
        }
    }

    private void setupNumberOfPlayers() {
        String number = numberOfPlayers.getText().toString();
        String[] parts = number.split("/");
        Integer playerNum = null;
        try {
            playerNum = Integer.parseInt(parts[0]);
            playerNum--;
        } catch (Exception e) {
        }
        if (playerNum != null) {
            numberOfPlayers.setText(playerNum + "/" + parts[1]);
        }
    }

    @Override
    public void onError(String s, String reason) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        Log.i(TAG, "onError: " + reason);
        if ("{\"error\":\"game_started\"}".equals(reason)) {
            status.setText("Game status: STARTED");
            joinLeaveButton.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onChatFragmentMessage(String message) {
        gameController.shout(gameInstance.getId(), message);
    }
}
