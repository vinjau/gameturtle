package rs.ac.bg.etf.student.pm130681d.gameturtle.view.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import rs.ac.bg.etf.student.pm130681d.gameturtle.R;
import rs.ac.bg.etf.student.pm130681d.gameturtle.controller.ApiInterface;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.GameInstance;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Player;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.PlayerType;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class GameInstanceListAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater = null;
    private static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.GERMANY);
    private List<GameInstance> gameInstances;

    public GameInstanceListAdapter(Context context, List<GameInstance> gameInstances) {
        this.context = context;
        this.gameInstances = gameInstances;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return gameInstances.size();
    }

    @Override
    public Object getItem(int position) {
        return gameInstances.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.game_list_item, null);

        GameInstance gameInstance = gameInstances.get(position);

        if (gameInstance != null) {
            TextView hostView = (TextView) vi.findViewById(R.id.hostName);
            TextView gameName = (TextView) vi.findViewById(R.id.gameName);
            ImageView imageView = (ImageView) vi.findViewById(R.id.gameImage);
            try {
                if (gameInstance.getGame().getMedias().get(0).getMedia() != null) {
                    Glide.with(context)
                            .load(ApiInterface.API_URL + gameInstance.getGame().getMedias().get(0).getMedia())
                            .into(imageView);
                }
            } catch (Exception e) {
            }
            for (Player player : gameInstance.getPlayers()) {
                if (player.getType().equals(PlayerType.HOST)) {
                    hostView.setText(player.getUserId().getUsername());
                    break;
                }
            }
            gameName.setText(gameInstance.getGame().getName());
            /*Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(gameInstance.get);*/
            TextView date = (TextView) vi.findViewById(R.id.gameDate);
            date.setText(dateFormat.format(gameInstance.getDate()));
            TextView playersNumber = (TextView) vi.findViewById(R.id.listItemNumberOfPlayers);
            playersNumber.setText(gameInstance.getPlayers().size() + "/" + gameInstance.getGame().getMaxPlayers());
        }

        return vi;
    }
}

