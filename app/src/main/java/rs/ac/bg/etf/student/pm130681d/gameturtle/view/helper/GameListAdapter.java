package rs.ac.bg.etf.student.pm130681d.gameturtle.view.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import rs.ac.bg.etf.student.pm130681d.gameturtle.R;
import rs.ac.bg.etf.student.pm130681d.gameturtle.controller.ApiInterface;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Game;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class GameListAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater = null;
    private static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.GERMANY);
    private List<Game> games;

    public GameListAdapter(Context context, List<Game> games) {
        this.context = context;
        this.games = games;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return games.size();
    }

    @Override
    public Object getItem(int position) {
        return games.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.game_list_item, null);

        Game game = games.get(position);

        if (game != null) {
            TextView gameName = (TextView) vi.findViewById(R.id.gameName);
            ImageView imageView = (ImageView) vi.findViewById(R.id.gameImage);
            try {
                if (game.getMedias().get(0).getMedia() != null) {
                    Glide.with(context)
                            .load(ApiInterface.API_URL + game.getMedias().get(0).getMedia())
                            .into(imageView);
                }
            } catch (Exception e) {
            }
            gameName.setText(game.getName());
            TextView playersNumber = (TextView) vi.findViewById(R.id.listItemNumberOfPlayers);
            playersNumber.setText(game.getMinPlayers() + "-" + game.getMaxPlayers());
        }

        return vi;
    }
}

