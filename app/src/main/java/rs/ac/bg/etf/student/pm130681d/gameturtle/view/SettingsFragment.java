package rs.ac.bg.etf.student.pm130681d.gameturtle.view;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import rs.ac.bg.etf.student.pm130681d.gameturtle.R;
import rs.ac.bg.etf.student.pm130681d.gameturtle.controller.UserController;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.GameInstance;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.User;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Vote;

/**
 * Created by TajnaSluzba on 24.9.2017.
 */

public class SettingsFragment extends PreferenceFragment implements UserController.ViewInterface {

    private static final String TAG = "SettingsFragment";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_general);

        final UserController userController = new UserController(getActivity(), this);

        Preference preference = getPreferenceManager().findPreference("password");
        preference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                Log.d(TAG, "onPreferenceChange:  PASSWORD");
                return true;
            }
        });

        Preference emailPreference = getPreferenceManager().findPreference("email");
        emailPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                String email = (String) o;
                if (email.contains("@")) {
                    userController.update("", email);
                } else {
                    Toast.makeText(getActivity(), "Not email", Toast.LENGTH_LONG).show();
                }
                return true;
            }
        });
    }

    @Override
    public void onRegister() {

    }

    @Override
    public void onLogin() {

    }

    @Override
    public void onUpdate() {

    }

    @Override
    public void onGetUser(User user) {

    }

    @Override
    public void onGetPastGames(List<GameInstance> games) {

    }

    @Override
    public void onGetCurrentGames(List<GameInstance> games) {

    }

    @Override
    public void onGetUserVotes(List<Vote> votes) {

    }

    @Override
    public void onUserVote() {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFail() {

    }
}
