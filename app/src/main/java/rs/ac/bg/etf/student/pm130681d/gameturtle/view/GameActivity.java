package rs.ac.bg.etf.student.pm130681d.gameturtle.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Date;
import java.util.List;

import rs.ac.bg.etf.student.pm130681d.gameturtle.R;
import rs.ac.bg.etf.student.pm130681d.gameturtle.controller.ApiInterface;
import rs.ac.bg.etf.student.pm130681d.gameturtle.controller.GameController;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Game;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.GameInstance;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.ShoutBox;
import rs.ac.bg.etf.student.pm130681d.gameturtle.view.dialogs.HostGameDialog;

public class GameActivity extends AppCompatActivity implements
        HostGameDialog.OnClickListener,
        GameController.ViewInterface {

    public static final String GAME = "gameturtle.view.GameActivity.GAME";

    private Game game;
    private GameController gameController;

    private TextView description;
    private TextView gameName;
    private TextView numberOfPlayers;
    private Button hostGame;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        game = (Game) getIntent().getSerializableExtra(GAME);
        if (game == null) {
            finish();
        }
        gameController = new GameController(this, this);

        description = (TextView) findViewById(R.id.singleGameDescription);
        gameName = (TextView) findViewById(R.id.singleGameName);
        numberOfPlayers = (TextView) findViewById(R.id.singleGameNumberOfPlayers);
        hostGame = (Button) findViewById(R.id.singleGameHost);
        imageView = (ImageView) findViewById(R.id.singleGameImage);

        try {
            Glide.with(this).load(ApiInterface.API_URL + game.getMedias().get(0).getMedia()).into(imageView);
        } catch (Exception e) {
        }

        final HostGameDialog hostGameDialog = new HostGameDialog(this , this, game.getId());

        description.setText(game.getDescription());
        gameName.setText(game.getName());
        numberOfPlayers.setText(game.getMinPlayers() + "-" + game.getMaxPlayers());
        hostGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hostGameDialog.show();
            }
        });
    }

    @Override
    public void onSearchBoardGames(List<Game> games) {

    }

    @Override
    public void onGetGames(List<GameInstance> games) {

    }

    @Override
    public void onGameHosted() {
        finish();
    }

    @Override
    public void onGameStarted() {

    }

    @Override
    public void onGameFinished() {

    }

    @Override
    public void onGameJoined() {

    }

    @Override
    public void onGameLeave() {

    }

    @Override
    public void onGameShout() {

    }

    @Override
    public void onGetGame(GameInstance gameInstance) {

    }

    @Override
    public void onGetShouts(List<ShoutBox> body) {

    }

    @Override
    public void onKickPlayer(int playerId) {

    }

    @Override
    public void onError(String s, String reason) {

    }

    @Override
    public void hostGame(int id, String place, Date date) {
        if (gameController != null) {
            gameController.host(id, place, date);
        }
    }
}
