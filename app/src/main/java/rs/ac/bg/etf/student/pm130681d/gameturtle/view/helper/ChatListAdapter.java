package rs.ac.bg.etf.student.pm130681d.gameturtle.view.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import rs.ac.bg.etf.student.pm130681d.gameturtle.R;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Player;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.ShoutBox;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class ChatListAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater = null;
    private static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.GERMANY);
    private List<ShoutBox> messages;

    public ChatListAdapter(Context context, List<ShoutBox> messages) {
        this.context = context;
        this.messages = messages;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.chat_list_item, null);

        ShoutBox shoutBox = messages.get(position);

        if (shoutBox != null) {
            TextView name = (TextView) vi.findViewById(R.id.chatName);
            TextView date = (TextView) vi.findViewById(R.id.chatDate);
            TextView message = (TextView) vi.findViewById(R.id.chatMessage);
            name.setText(shoutBox.getPlayer().getUserId().getUsername());
            date.setText(dateFormat.format(shoutBox.getDate()));
            message.setText(shoutBox.getComment());
        }

        return vi;
    }
}

