package rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public enum VoteStatus {
    PENDING, VOTED
}
