package rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper;

import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Player;

/**
 * Created by TajnaSluzba on 24.9.2017.
 */

public class PlayerVoteIndicatorWrapper {
    private Player player;
    private boolean voted;

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public boolean isVoted() {
        return voted;
    }

    public void setVoted(boolean voted) {
        this.voted = voted;
    }
}
