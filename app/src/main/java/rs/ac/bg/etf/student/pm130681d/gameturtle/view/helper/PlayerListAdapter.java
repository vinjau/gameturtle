package rs.ac.bg.etf.student.pm130681d.gameturtle.view.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import rs.ac.bg.etf.student.pm130681d.gameturtle.R;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.GameInstance;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Player;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.PlayerType;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class PlayerListAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater = null;
    private List<Player> players;

    public PlayerListAdapter(Context context, List<Player> players) {
        this.context = context;
        this.players = players;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int position) {
        return players.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.player_list_item, null);

        Player player = players.get(position);

        if (player != null) {
            TextView name = (TextView) vi.findViewById(R.id.playerListName);
            TextView minus = (TextView) vi.findViewById(R.id.playerListMinus);
            TextView plus = (TextView) vi.findViewById(R.id.playerListNamePlus);
            name.setText(player.getUserId().getUsername());
            plus.setText(player.getUserId().getPositiveVotes() + "");
            minus.setText(player.getUserId().getNegativeVotes() + "");
        }

        return vi;
    }
}

