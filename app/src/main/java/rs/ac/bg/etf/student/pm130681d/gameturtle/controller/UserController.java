package rs.ac.bg.etf.student.pm130681d.gameturtle.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.GameInstance;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.User;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Vote;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.AccessToken;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.GameInstanceActivityResponse;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.GameInstanceBody;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.UserUpdateResponse;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class UserController {

    public interface ViewInterface {
        void onRegister();
        void onLogin();
        void onUpdate();
        void onGetUser(User user);
        void onGetPastGames(List<GameInstance> games);
        void onGetCurrentGames(List<GameInstance> games);
        void onGetUserVotes(List<Vote> votes);
        void onUserVote();
        void onError();
        void onFail();
    }

    private static final String TAG = "UserController";
    public static final String USERNAME = "gameturtle.controller.UserController.username";
    public static final String ACCESS_TOKEN = "gameturtle.controller.UserController.accessToken";

    private Context context;
    private ViewInterface viewInterface;

    public UserController(Context context, ViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }

    public void register(final String username, String password) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setApplicationToken(ApiInterface.APP_TOKEN_CODE);
        user.setEmail("");
        final Call<AccessToken> register = ApiClient.getApiInterface().registerUser(user);
        register.enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                try {
                    if (response.errorBody() != null) {
                        Log.d(TAG, "registracija_GR:" + response.errorBody().string().toString());
                        viewInterface.onError();
                    }
                    if (response.body() != null) {
                        Log.d(TAG, "registracija:" + response.body().getAccessToken());
                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
                        sp.edit()
                                .putString(USERNAME, username)
                                .putString(ACCESS_TOKEN, response.body().getAccessToken())
                                .apply();
                        viewInterface.onRegister();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<AccessToken> call, Throwable t) {
                Log.d(TAG, "REG:GRESKA!");
                viewInterface.onFail();
            }
        });
    }

    public void login(final String username, String password) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setApplicationToken(ApiInterface.APP_TOKEN_CODE);
        user.setEmail("");
        final Call<AccessToken> loginCall = ApiClient.getApiInterface().loginUser(user);
        loginCall.enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                try {
                    if (response.errorBody() != null) {
                        Log.i(TAG, "login_GR:" + response.errorBody().string().toString());
                        viewInterface.onError();
                    }
                    if (response.body() != null) {
                        Log.i(TAG, "login:" + response.body().getAccessToken());
                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
                        sp.edit()
                                .putString(USERNAME, username)
                                .putString(ACCESS_TOKEN, response.body().getAccessToken())
                                .apply();
                        viewInterface.onLogin();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<AccessToken> call, Throwable t) {
                Log.d(TAG, "LOGIN:GRESKA!");
                viewInterface.onFail();
            }
        });
    }

    public void update(String password, String email) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        final String username = sp.getString(USERNAME, "");
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setAccessToken(sp.getString(ACCESS_TOKEN, ""));
        user.setEmail(email);
        final Call<UserUpdateResponse> updateCall = ApiClient.getApiInterface().updateUser(user);
        updateCall.enqueue(new Callback<UserUpdateResponse>() {
            @Override
            public void onResponse(Call<UserUpdateResponse> call, Response<UserUpdateResponse> response) {
                try {
                    if (response.errorBody() != null) {
                        Log.i(TAG, "update_GR:" + response.errorBody().string().toString());
                        Toast.makeText(context, "Error updating email", Toast.LENGTH_LONG).show();
                    }
                    if (response.body() != null) {
                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
                        if (response.body().getAccessToken() != null) {
                            Log.i(TAG, "update:" + response.body().getAccessToken());
                            sp.edit()
                                    .putString(USERNAME, username)
                                    .putString(ACCESS_TOKEN, response.body().getAccessToken())
                                    .apply();
                            viewInterface.onUpdate();
                            Toast.makeText(context, "Password updated", Toast.LENGTH_LONG).show();
                        }
                        if (response.body().getEmail() != null) {
                            Log.d(TAG, "onResponse: update email");
                            sp.edit().putString("email", response.body().getEmail()).apply();
                            Toast.makeText(context, "Email updated", Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<UserUpdateResponse> call, Throwable t) {
                Log.d(TAG, "LOGIN:GRESKA!");
                Toast.makeText(context, "Error updating email", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getUser() {
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        final String username = sp.getString(USERNAME, "");
        final String accessToken = sp.getString(ACCESS_TOKEN, "");
        Call<User> updateCall = ApiClient.getApiInterface().getUser(username, accessToken);
        updateCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                try {
                    if (response.errorBody() != null) {
                        Log.i(TAG, "getUser_GR:" + response.errorBody().string());
                        viewInterface.onError();
                    }
                    if (response.body() != null) {
                        Log.i(TAG, "getUser:" + response.body().getUsername());
                        viewInterface.onGetUser(response.body());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d(TAG, "getUser:GRESKA!");
                viewInterface.onFail();
            }
        });
    }

    public void getCurrentGames() {
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        final String username = sp.getString(USERNAME, "");
        final String accessToken = sp.getString(ACCESS_TOKEN, "");
        Call<List<GameInstance>> currentGamesCall = ApiClient.getApiInterface().getCurrentGames(username, accessToken);
        currentGamesCall.enqueue(new Callback<List<GameInstance>>() {
            @Override
            public void onResponse(Call<List<GameInstance>> call, Response<List<GameInstance>> response) {
                Log.d(TAG, "onResponse: getCurrentGames");
                if (response.body() != null) {
                    viewInterface.onGetCurrentGames(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<GameInstance>> call, Throwable t) {
                Log.d(TAG, "onFailure: getCurrentGames");
            }
        });
    }

    public void getPastGames() {
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        final String username = sp.getString(USERNAME, "");
        final String accessToken = sp.getString(ACCESS_TOKEN, "");
        Call<List<GameInstance>> pastGamesCall = ApiClient.getApiInterface().getPastGames(username, accessToken);
        pastGamesCall.enqueue(new Callback<List<GameInstance>>() {
            @Override
            public void onResponse(Call<List<GameInstance>> call, Response<List<GameInstance>> response) {
                Log.d(TAG, "onResponse: getPastGames");
                if (response.body() != null) {
                    viewInterface.onGetPastGames(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<GameInstance>> call, Throwable t) {
                Log.d(TAG, "onFailure: getPastGames");
            }
        });
    }

    public void getUserVote(int gameInstanceId) {
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        final String username = sp.getString(USERNAME, "");
        final String accessToken = sp.getString(ACCESS_TOKEN, "");
        String gameInstanceIdString = gameInstanceId + "";
        Call<List<Vote>> callGetVotes = ApiClient.getApiInterface().getVotesForUser(username, accessToken, gameInstanceIdString);
        callGetVotes.enqueue(new Callback<List<Vote>>() {
            @Override
            public void onResponse(Call<List<Vote>> call, Response<List<Vote>> response) {
                if (response.errorBody() != null) {
                    Log.d(TAG, "onResponse: getUserVote ERROR");
                }
                if (response.body() != null) {
                    viewInterface.onGetUserVotes(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Vote>> call, Throwable t) {
                Log.d(TAG, "onFailure: getUserVote");
            }
        });
    }

    public void vote(int gameInstanceId, int vote, int playerId) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        GameInstanceBody gameInstanceBody = new GameInstanceBody();
        gameInstanceBody.setUsername(sp.getString(UserController.USERNAME, ""));
        gameInstanceBody.setAccessToken(sp.getString(UserController.ACCESS_TOKEN, ""));
        gameInstanceBody.setGameInstanceId(gameInstanceId + "");
        gameInstanceBody.setPlayerId(playerId + "");
        gameInstanceBody.setVote(vote + "");
        Call<GameInstanceActivityResponse> hostCall = ApiClient.getApiInterface().vote(gameInstanceBody);
        hostCall.enqueue(new Callback<GameInstanceActivityResponse>() {
            @Override
            public void onResponse(Call<GameInstanceActivityResponse> call, Response<GameInstanceActivityResponse> response) {
                Log.i(TAG, "onResponse: vote");
                if (response.body() != null) {
                    // error checking with response.body().getPlayer() || getGame()
                    viewInterface.onUserVote();
                }
            }

            @Override
            public void onFailure(Call<GameInstanceActivityResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: vote");
            }
        });
    }
}
