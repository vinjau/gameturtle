package rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class GameInstanceActivityResponse {

    @SerializedName("player")
    private String player;

    @SerializedName("game")
    private String game;

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }
}
