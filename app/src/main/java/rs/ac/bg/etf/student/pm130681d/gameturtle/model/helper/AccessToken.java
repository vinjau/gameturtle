package rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class AccessToken {

    @SerializedName("access_token")
    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
