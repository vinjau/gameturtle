package rs.ac.bg.etf.student.pm130681d.gameturtle.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Game;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.GameInstance;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.ShoutBox;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.GameInstanceActivityResponse;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.GameInstanceBody;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class GameController {

    public interface ViewInterface {
        void onSearchBoardGames(List<Game> games);
        void onGetGames(List<GameInstance> games);
        void onGameHosted();
        void onGameStarted();
        void onGameFinished();
        void onGameJoined();
        void onGameLeave();
        void onGameShout();
        void onGetGame(GameInstance gameInstance);
        void onGetShouts(List<ShoutBox> body);
        void onKickPlayer(int playerId);

        void onError(String s, String reason);
    }

    private static final String TAG = "GameController";
    private static final DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy", Locale.GERMANY);

    private Context context;
    private ViewInterface viewInterface;

    public GameController(Context context, ViewInterface viewInterface) {
        this.context = context;
        this.viewInterface = viewInterface;
    }

    public void searchBoardGames(String gameName) {
        Call<List<Game>> gameCall = ApiClient.getApiInterface().searchBoardGames(gameName);
        gameCall.enqueue(new Callback<List<Game>>() {
            @Override
            public void onResponse(Call<List<Game>> call, Response<List<Game>> response) {
                if (response.body() != null) {
                    Log.d(TAG, "onResponse: seachBoardGames");
                    viewInterface.onSearchBoardGames(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Game>> call, Throwable t) {
                Log.d(TAG, "onFailure: getGames");
            }
        });
    }

    public void getGames(@Nullable String gameName) {
        Call<List<GameInstance>> gameCall = ApiClient
                                            .getApiInterface()
                                            .getGames(gameName);
        gameCall.enqueue(new Callback<List<GameInstance>>() {
            @Override
            public void onResponse(
                    Call<List<GameInstance>> call,
                    Response<List<GameInstance>> response) {
                if (response.body() != null) {
                    Log.d(TAG, "onResponse: getGames");
                    viewInterface.onGetGames(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<GameInstance>> call, Throwable t) {
                Log.d(TAG, "onFailure: getGames");
            }
        });
    }

    public void getGame(int gameInstanceId) {
        Call<GameInstance> gameInstanceCall = ApiClient.getApiInterface().getGame(gameInstanceId);
        gameInstanceCall.enqueue(new Callback<GameInstance>() {
            @Override
            public void onResponse(Call<GameInstance> call, Response<GameInstance> response) {
                Log.d(TAG, "onResponse: getGame");
                if (response.body() != null) {
                    viewInterface.onGetGame(response.body());
                }
            }

            @Override
            public void onFailure(Call<GameInstance> call, Throwable t) {
                Log.d(TAG, "onFailure: getGame");
            }
        });
    }

    public void host(int gameId, String place, Date date) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        GameInstanceBody gameInstanceBody = new GameInstanceBody();
        gameInstanceBody.setUsername(sp.getString(UserController.USERNAME, ""));
        gameInstanceBody.setAccessToken(sp.getString(UserController.ACCESS_TOKEN, ""));
        gameInstanceBody.setGameId(gameId + "");
        gameInstanceBody.setPlace(place);
        gameInstanceBody.setDate(dateFormat.format(date));
        Call<GameInstanceActivityResponse> hostCall = ApiClient.getApiInterface().hostGame(gameInstanceBody);
        hostCall.enqueue(new Callback<GameInstanceActivityResponse>() {
            @Override
            public void onResponse(Call<GameInstanceActivityResponse> call, Response<GameInstanceActivityResponse> response) {
                Log.i(TAG, "onResponse: host");
                if (response.body() != null) {
                    // error checking with response.body().getPlayer() || getGame()
                    viewInterface.onGameHosted();
                }
            }

            @Override
            public void onFailure(Call<GameInstanceActivityResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: host");
            }
        });
    }

    public void kickPlayer(final int playerId) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        GameInstanceBody gameInstanceBody = new GameInstanceBody();
        gameInstanceBody.setUsername(sp.getString(UserController.USERNAME, ""));
        gameInstanceBody.setAccessToken(sp.getString(UserController.ACCESS_TOKEN, ""));
        gameInstanceBody.setPlayerId(playerId + "");
        Call<GameInstanceActivityResponse> kickCall = ApiClient.getApiInterface().kickPlayer(gameInstanceBody);
        kickCall.enqueue(new Callback<GameInstanceActivityResponse>() {
            @Override
            public void onResponse(Call<GameInstanceActivityResponse> call, Response<GameInstanceActivityResponse> response) {
                Log.i(TAG, "onResponse: host");
                if (response.body() != null) {
                    // error checking with response.body().getPlayer() || getGame()
                    viewInterface.onKickPlayer(playerId);
                }
            }

            @Override
            public void onFailure(Call<GameInstanceActivityResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: host");
            }
        });
    }

    public void start(int gameInstanceId) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        GameInstanceBody gameInstanceBody = new GameInstanceBody();
        gameInstanceBody.setUsername(sp.getString(UserController.USERNAME, ""));
        gameInstanceBody.setAccessToken(sp.getString(UserController.ACCESS_TOKEN, ""));
        gameInstanceBody.setGameInstanceId(gameInstanceId + "");
        Call<GameInstanceActivityResponse> hostCall = ApiClient.getApiInterface().startGame(gameInstanceBody);
        hostCall.enqueue(new Callback<GameInstanceActivityResponse>() {
            @Override
            public void onResponse(Call<GameInstanceActivityResponse> call, Response<GameInstanceActivityResponse> response) {
                Log.i(TAG, "onResponse: start");
                if (response.body() != null) {
                    // error checking with response.body().getPlayer() || getGame()
                    viewInterface.onGameStarted();
                }
                if (response.errorBody() != null) {
                    try {
                        if (response.errorBody().string().equals("{\"error\":\"need_more_players\"}")) {
                            Toast.makeText(context, "Need more players to start game!", Toast.LENGTH_LONG ).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<GameInstanceActivityResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: start");
            }
        });
    }

    public void finish(int gameInstanceId) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        GameInstanceBody gameInstanceBody = new GameInstanceBody();
        gameInstanceBody.setUsername(sp.getString(UserController.USERNAME, ""));
        gameInstanceBody.setAccessToken(sp.getString(UserController.ACCESS_TOKEN, ""));
        gameInstanceBody.setGameInstanceId(gameInstanceId + "");
        Call<GameInstanceActivityResponse> hostCall = ApiClient.getApiInterface().finishGame(gameInstanceBody);
        hostCall.enqueue(new Callback<GameInstanceActivityResponse>() {
            @Override
            public void onResponse(Call<GameInstanceActivityResponse> call, Response<GameInstanceActivityResponse> response) {
                Log.i(TAG, "onResponse: finish");
                if (response.body() != null) {
                    // error checking with response.body().getPlayer() || getGame()
                    viewInterface.onGameFinished();
                }
            }

            @Override
            public void onFailure(Call<GameInstanceActivityResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: finish");
            }
        });
    }

    public void join(int gameInstanceId) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        GameInstanceBody gameInstanceBody = new GameInstanceBody();
        gameInstanceBody.setUsername(sp.getString(UserController.USERNAME, ""));
        gameInstanceBody.setAccessToken(sp.getString(UserController.ACCESS_TOKEN, ""));
        gameInstanceBody.setGameInstanceId(gameInstanceId + "");
        Call<GameInstanceActivityResponse> hostCall = ApiClient.getApiInterface().joinGame(gameInstanceBody);
        hostCall.enqueue(new Callback<GameInstanceActivityResponse>() {
            @Override
            public void onResponse(Call<GameInstanceActivityResponse> call, Response<GameInstanceActivityResponse> response) {
                Log.i(TAG, "onResponse: join");
                if (response.body() != null) {
                    // error checking with response.body().getPlayer() || getGame()
                    viewInterface.onGameJoined();
                }
            }

            @Override
            public void onFailure(Call<GameInstanceActivityResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: join");
            }
        });
    }

    public void leave(int gameInstanceId) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        GameInstanceBody gameInstanceBody = new GameInstanceBody();
        gameInstanceBody.setUsername(sp.getString(UserController.USERNAME, ""));
        gameInstanceBody.setAccessToken(sp.getString(UserController.ACCESS_TOKEN, ""));
        gameInstanceBody.setGameInstanceId(gameInstanceId + "");
        Call<GameInstanceActivityResponse> hostCall = ApiClient.getApiInterface().leaveGame(gameInstanceBody);
        hostCall.enqueue(new Callback<GameInstanceActivityResponse>() {
            @Override
            public void onResponse(Call<GameInstanceActivityResponse> call, Response<GameInstanceActivityResponse> response) {
                Log.d(TAG, "onResponse: leave");
                if (response.body() != null) {
                    // error checking with response.body().getPlayer() || getGame()
                    viewInterface.onGameLeave();
                }
                if (response.errorBody() != null) {
                    try {
                        viewInterface.onError("Can't leave game", response.errorBody().string().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<GameInstanceActivityResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: leave");
            }
        });
    }

    public void shout(int gameInstanceId, String message) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        GameInstanceBody gameInstanceBody = new GameInstanceBody();
        gameInstanceBody.setUsername(sp.getString(UserController.USERNAME, ""));
        gameInstanceBody.setAccessToken(sp.getString(UserController.ACCESS_TOKEN, ""));
        gameInstanceBody.setGameInstanceId(gameInstanceId + "");
        gameInstanceBody.setMessage(message);
        Call<GameInstanceActivityResponse> hostCall = ApiClient.getApiInterface().shout(gameInstanceBody);
        hostCall.enqueue(new Callback<GameInstanceActivityResponse>() {
            @Override
            public void onResponse(Call<GameInstanceActivityResponse> call, Response<GameInstanceActivityResponse> response) {
                Log.i(TAG, "onResponse: shout");
                if (response.body() != null) {
                    // error checking with response.body().getPlayer() || getGame()
                    viewInterface.onGameShout();
                }
            }

            @Override
            public void onFailure(Call<GameInstanceActivityResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: shout");
            }
        });
    }

    public void getShouts(final int gameInstanceId) {
        Call<List<ShoutBox>> shoutsCall = ApiClient.getApiInterface().getShouts(gameInstanceId);
        shoutsCall.enqueue(new Callback<List<ShoutBox>>() {
            @Override
            public void onResponse(Call<List<ShoutBox>> call, Response<List<ShoutBox>> response) {
                Log.d(TAG, "onResponse: getShouts(" + gameInstanceId + ")");
                if (response.body() != null) {
                    viewInterface.onGetShouts(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<ShoutBox>> call, Throwable t) {
                Log.d(TAG, "onFailure: getShouts(" + ")");
                Toast.makeText(context, "Error! Try again later." , Toast.LENGTH_LONG).show();
            }
        });
    }
}
