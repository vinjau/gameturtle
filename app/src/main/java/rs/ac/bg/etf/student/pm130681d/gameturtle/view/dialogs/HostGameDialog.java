package rs.ac.bg.etf.student.pm130681d.gameturtle.view.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import rs.ac.bg.etf.student.pm130681d.gameturtle.R;

/**
 * Created by TajnaSluzba on 23.9.2017.
 */

public class HostGameDialog implements
        DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener,
        DialogInterface.OnCancelListener {

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        year = i;
        month = i1;
        day = i2;

        calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(activity, this, hour, minute,
                DateFormat.is24HourFormat(activity));
        timePickerDialog.show();
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1) {
        hour = i;
        minute = i1;

        calendar.set(year, month, day, hour, minute);
        java.text.DateFormat dateFormat = new SimpleDateFormat("HH:mm dd-MM-yyyy", Locale.GERMANY);
        hostDate.setText(dateFormat.format(calendar.getTime()));
    }

    public interface OnClickListener {
        void hostGame(int id, String place, Date date);
    }

    private AlertDialog dialog;
    private Activity activity;
    private OnClickListener onClickListener;

    private TextView hostDate;

    private int gameId;
    private int hour;
    private int minute;
    private int day;
    private int month;
    private int year;
    private Calendar calendar;



    private DialogInterface.OnClickListener dialogOnClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int choice) {
            if (choice == AlertDialog.BUTTON_POSITIVE) {
                if (calendar != null) {
                    TextView hostPlace = (TextView) dialog.findViewById(R.id.hostPlace);
                    onClickListener.hostGame(gameId, hostPlace.getText().toString(), calendar.getTime());
                }
            }
        }
    };

    public HostGameDialog(final Activity activity, OnClickListener onClickListener, int gameId) {
        this.gameId = gameId;
        this.activity = activity;
        this.onClickListener = onClickListener;
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.host_game_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(dialogView);
        builder.setTitle("Host new game");
        builder.setPositiveButton("OK", dialogOnClickListener);
        builder.setNegativeButton("CANCEL", dialogOnClickListener);
        builder.setOnCancelListener(this);
        dialog = builder.create();

        hostDate = (TextView) dialogView.findViewById(R.id.hostDateText);
        final Button dateButton = (Button) dialogView.findViewById(R.id.hostDateButton);
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(activity, HostGameDialog.this,
                        year, month, day);
                datePickerDialog.show();
            }
        });
    }

    public void show() {
        dialog.show();
    }

    public void dismiss() {
        dialog.dismiss();
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {

    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        public static int hour;
        public static int min;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        }
    }
}
