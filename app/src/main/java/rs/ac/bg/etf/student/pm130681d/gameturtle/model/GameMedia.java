package rs.ac.bg.etf.student.pm130681d.gameturtle.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.GameMediaType;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class GameMedia implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("media")
    private String media;

    @SerializedName("type")
    private GameMediaType type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public GameMediaType getType() {
        return type;
    }

    public void setType(GameMediaType type) {
        this.type = type;
    }
}
