package rs.ac.bg.etf.student.pm130681d.gameturtle.view.helper;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import rs.ac.bg.etf.student.pm130681d.gameturtle.R;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.User;

public class MyAccountActivity extends AppCompatActivity {

    public static final String USER = "gameturtle.view.helper.MyAccountActivity.USER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);

        User user = (User) getIntent().getSerializableExtra(USER);

        TextView username = (TextView) findViewById(R.id.myAccountUsername);
        TextView email = (TextView) findViewById(R.id.myAccountEmail);
        TextView positiveVotes = (TextView) findViewById(R.id.myAccountPositive);
        TextView negativeVotes = (TextView) findViewById(R.id.myAccountNegative);
        username.setText(user.getUsername());
        email.setText(user.getEmail());
        positiveVotes.setText(user.getPositiveVotes() + "");
        negativeVotes.setText(user.getNegativeVotes() + "");
    }
}
