package rs.ac.bg.etf.student.pm130681d.gameturtle.controller;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class ApiClient {

    private static ApiInterface apiInterface;

    public static ApiInterface getApiInterface() {
        if (apiInterface == null) {
            Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiInterface.API_URL)
                    .addConverterFactory(GsonConverterFactory.create()).build();
            apiInterface = retrofit.create(ApiInterface.class);
        }
        return apiInterface;
    }
}
