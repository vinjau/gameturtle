package rs.ac.bg.etf.student.pm130681d.gameturtle.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class ShoutBox implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("comment")
    private String comment;

    @SerializedName("playerId")
    private Player player;

    @SerializedName("createdOn")
    private Long date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }
}
