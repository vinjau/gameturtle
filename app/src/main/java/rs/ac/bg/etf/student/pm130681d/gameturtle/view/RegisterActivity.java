package rs.ac.bg.etf.student.pm130681d.gameturtle.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import java.util.List;
import java.util.zip.Inflater;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ac.bg.etf.student.pm130681d.gameturtle.R;
import rs.ac.bg.etf.student.pm130681d.gameturtle.controller.ApiClient;
import rs.ac.bg.etf.student.pm130681d.gameturtle.controller.ApiInterface;
import rs.ac.bg.etf.student.pm130681d.gameturtle.controller.UserController;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Game;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.GameInstance;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.ShoutBox;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.User;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Vote;

public class RegisterActivity extends AppCompatActivity implements UserController.ViewInterface {

    private static final String TAG = "RegisterActivity";

    private boolean loginMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final TextView username = (TextView) findViewById(R.id.username);
        final TextView password = (TextView) findViewById(R.id.password);
        final TextView repeatPassword = (TextView) findViewById(R.id.repeatPassword);
        final UserController userController = new UserController(this, this);

        final Button register = (Button) findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (username.getText().toString().length() > 0 && password.getText().toString().length() > 0) {
                    if (!loginMode &&  password.getText().toString().equals(repeatPassword.getText().toString())) {
                        userController.register(username.getText().toString(), password.getText().toString());
                    } else if (loginMode) {
                        userController.login(username.getText().toString(), password.getText().toString());
                    }
                } else {
                    Toast.makeText(RegisterActivity.this, "Greska pokusajte ponovo.", Toast.LENGTH_LONG).show();
                }
            }
        });

        final TextView loginText = (TextView) findViewById(R.id.alreadyRegistred);
        loginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (loginMode) {
                    repeatPassword.setVisibility(View.VISIBLE);
                    register.setText("REGISTER");
                    loginText.setText("I am already registred. Login me.");
                    loginMode = false;
                } else {
                    repeatPassword.setVisibility(View.INVISIBLE);
                    register.setText("LOGIN");
                    loginText.setText("No account? Register here.");
                    loginMode = true;
                }
            }
        });
    }

    @Override
    public void onRegister() {
        Intent intent = new Intent(getBaseContext(), GameListActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onLogin() {
        Intent intent = new Intent(getBaseContext(), GameListActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUpdate() {

    }

    @Override
    public void onGetUser(User user) {

    }

    @Override
    public void onGetPastGames(List<GameInstance> games) {

    }

    @Override
    public void onGetCurrentGames(List<GameInstance> games) {

    }

    @Override
    public void onGetUserVotes(List<Vote> votes) {

    }

    @Override
    public void onUserVote() {

    }

    @Override
    public void onError() {
        Toast.makeText(this, "Greska pokusajte ponovo.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFail() {
        Toast.makeText(this, "Fail", Toast.LENGTH_LONG).show();
    }
}
