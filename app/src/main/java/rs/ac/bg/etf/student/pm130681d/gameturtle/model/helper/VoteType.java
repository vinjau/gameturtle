package rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public enum VoteType {

    NEGATIVE(-1), PENDING(0), POSITIVE(1);

    private final int value;

    VoteType(final int value) {
        this.value = value;
    }

    public int getValue() { return value; }

    public static VoteType parse(int id) {
        VoteType vote = null;
        for (VoteType item : VoteType.values()) {
            if (item.getValue() == id) {
                vote = item;
                break;
            }
        }
        return vote;
    }
}
