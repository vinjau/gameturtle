package rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class GameInstanceBody {

    @SerializedName("username")
    private String username;

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("game_instance_id")
    private String gameInstanceId;

    @SerializedName("player_id")
    private String playerId;

    @SerializedName("vote")
    private String vote;

    @SerializedName("message")
    private String message;

    @SerializedName("game_id")
    private String gameId;

    @SerializedName("place")
    private String place;

    @SerializedName("date")
    private String date;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getGameInstanceId() {
        return gameInstanceId;
    }

    public void setGameInstanceId(String gameInstanceId) {
        this.gameInstanceId = gameInstanceId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getVote() {
        return vote;
    }

    public void setVote(String vote) {
        this.vote = vote;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
