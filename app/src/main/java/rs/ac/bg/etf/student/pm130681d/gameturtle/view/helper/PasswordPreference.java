package rs.ac.bg.etf.student.pm130681d.gameturtle.view.helper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.preference.DialogPreference;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import rs.ac.bg.etf.student.pm130681d.gameturtle.R;
import rs.ac.bg.etf.student.pm130681d.gameturtle.controller.UserController;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.GameInstance;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.User;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Vote;

/**
 * Created by TajnaSluzba on 24.9.2017.
 */

public class PasswordPreference extends DialogPreference implements UserController.ViewInterface {

    private static final String TAG = "PasswordPreference";
    private String password;
    private String check;
    private UserController userController;

    public PasswordPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        setDialogLayoutResource(R.layout.password_layout);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);

        setDialogIcon(null);
        userController = new UserController(getContext(), this);

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        EditText pass = (EditText) ((AlertDialog) dialog).findViewById(R.id.passPref);
        EditText checkPass = (EditText) ((AlertDialog) dialog).findViewById(R.id.confPref);
        password = pass.getText().toString();
        check = checkPass.getText().toString();
        super.onDismiss(dialog);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult) {
            if (password.equals(check) && !password.equals("")) {
                userController.update(password, "");
            } else {
                Toast.makeText(getContext(), "Password doesn't match", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRegister() {

    }

    @Override
    public void onLogin() {

    }

    @Override
    public void onUpdate() {

    }

    @Override
    public void onGetUser(User user) {

    }

    @Override
    public void onGetPastGames(List<GameInstance> games) {

    }

    @Override
    public void onGetCurrentGames(List<GameInstance> games) {

    }

    @Override
    public void onGetUserVotes(List<Vote> votes) {

    }

    @Override
    public void onUserVote() {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFail() {

    }
}
