package rs.ac.bg.etf.student.pm130681d.gameturtle.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.PlayerStatus;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.PlayerType;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public class Player implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("userId")
    private User userId;

    @SerializedName("status")
    private PlayerStatus status;

    @SerializedName("type")
    private PlayerType type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public PlayerStatus getStatus() {
        return status;
    }

    public void setStatus(PlayerStatus status) {
        this.status = status;
    }

    public PlayerType getType() {
        return type;
    }

    public void setType(PlayerType type) {
        this.type = type;
    }
}
