package rs.ac.bg.etf.student.pm130681d.gameturtle.controller;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Game;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.GameInstance;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.ShoutBox;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.User;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.Vote;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.AccessToken;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.GameInstanceActivityResponse;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.GameInstanceBody;
import rs.ac.bg.etf.student.pm130681d.gameturtle.model.helper.UserUpdateResponse;

/**
 * Created by TajnaSluzba on 22.9.2017.
 */

public interface ApiInterface {

    String APP_TOKEN_CODE = "NAGhL6l8NMersxFAnI/qo1xpCMpUKvxtVdB78EdeGyE=";
    String API_URL = "http://192.168.0.100:8000";
    // String API_URL = "http://snikiserver.xyz";

    @POST("/api/user/register")
    Call<AccessToken> registerUser(@Body User user);

    @POST("/api/user/login")
    Call<AccessToken> loginUser(@Body User user);

    @GET("/api/user")
    Call<User> getUser(
            @Query("username") String username,
            @Query("access_token") String accessToken
    );

    @POST("/api/user/update")
    Call<UserUpdateResponse> updateUser(@Body User user);

    @GET("/api/games")
    Call<List<Game>> searchBoardGames(@Query("name") String gameName);

    @GET("/api/games/list")
    Call<List<GameInstance>> getGames(@Query("name") String gameName);

    @GET("/api/game/{id}")
    Call<GameInstance> getGame(@Path("id") int id);

    @POST("/api/game/join")
    Call<GameInstanceActivityResponse> joinGame(@Body GameInstanceBody gameInstanceBody);

    @POST("/api/game/host")
    Call<GameInstanceActivityResponse> hostGame(@Body GameInstanceBody gameInstanceBody);

    @POST("/api/game/kick")
    Call<GameInstanceActivityResponse> kickPlayer(@Body GameInstanceBody gameInstanceBody);

    @POST("/api/game/start")
    Call<GameInstanceActivityResponse> startGame(@Body GameInstanceBody gameInstanceBody);

    @POST("/api/game/finish")
    Call<GameInstanceActivityResponse> finishGame(@Body GameInstanceBody gameInstanceBody);

    @POST("/api/game/leave")
    Call<GameInstanceActivityResponse> leaveGame(@Body GameInstanceBody gameInstanceBody);

    @POST("/api/game/shout")
    Call<GameInstanceActivityResponse> shout(@Body GameInstanceBody gameInstanceBody);

    @GET("/api/vote")
    Call<List<Vote>> getVotesForUser(
            @Query("username") String username,
            @Query("access_token") String accessToken,
            @Query("game_instance_id") String gameInstanceId
            );

    @POST("/api/game/vote")
    Call<GameInstanceActivityResponse> vote(@Body GameInstanceBody gameInstanceBody);

    // debug
    @GET("/api/game/shouts/{id}")
    Call<List<ShoutBox>> getShouts(@Path("id") int id);

    @GET("/api/user/past_games")
    Call<List<GameInstance>> getPastGames(
            @Query("username") String username,
            @Query("access_token") String accessToken
            );

    @GET("/api/user/current_games")
    Call<List<GameInstance>> getCurrentGames(
            @Query("username") String username,
            @Query("access_token") String accessToken
    );
}
